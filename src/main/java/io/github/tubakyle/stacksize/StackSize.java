package io.github.tubakyle.stacksize;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class StackSize extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        BukkitTask task = new enablerTask(this).runTaskLater(this, 2);
    }
    @EventHandler
    void onJoin(PlayerJoinEvent e) {
        setStackLimit(e.getPlayer());
    }
    void setStackLimit(Player p) {
        for (int i=1; i<65; i++) {
            if (p.hasPermission("stacksize." + Integer.toString(i))) {
                p.getInventory().setMaxStackSize(i);
            }
        }
    }
}