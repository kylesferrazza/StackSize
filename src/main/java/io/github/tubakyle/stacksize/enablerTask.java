package io.github.tubakyle.stacksize;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Kyle on 7/14/2014.
 */
public class enablerTask extends BukkitRunnable {

    private final JavaPlugin plugin;

    public enablerTask(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for (Player p : plugin.getServer().getOnlinePlayers()) {
            StackSize s = StackSize.getPlugin(StackSize.class);
            s.setStackLimit(p.getPlayer());
        }
    }
}
