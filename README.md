# StackSize

Minecraft bukkit plugin that restricts the stack sizes of items based on player permissions.

## Permissions
<pre>
stacksize.1
stacksize.2
</pre>
and so on, up to 64.
Each player should only have one of these permissions.
Whatever number follows 
<pre>stacksize.</pre>
will be the maximum size for any stack in that player's inventory.
<br> This only works in survival mode.
